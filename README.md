﻿# README #

### Assignment 1 JDBC ###

* School assignment, project implements functionality towards a database using JDBC.
* This application reads meta-data and data from a .txt file formats it and inserts it into a db. 

### Requirements ###

* To be able to run this project the jbdc.properties file need to be updated to match your current systems configurations.
* You will need a user with access to a db with permissions to perform default C.R.U.D operations. 
* The file to be read into the db "file.txt" needs to be in the following format:
* The first three rows contains metadata:  

1. Columnname1/Columnname2/Columnname3/Columnname4 etc..

2. Datatype1/Datatype2/Datatype3/Datatype4 etc..

3. Size1/Size2/Size3/Size4 etc..

4. The rest of the file contains the actual rows to be inserted. 

* Joakim Söderstrand/Hemlig/123456/29

* Test Person/Testgatan/1234567/37

* Per Person/Pergatan/1234569/34

* Anders Andersson/Andgatan/123412/39

* Felicia Nilsson/Kungsgatan/12326/20

### Documentation ###
* The project has been set-up with mavens javadoc plugin and can easily be generated with maven.