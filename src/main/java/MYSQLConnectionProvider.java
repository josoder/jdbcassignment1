import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class to maintain the datasource and to establish a connection.
 * @author Joakim Söderstrand 2016-11-02
 * @version 1.0
 */
public class MYSQLConnectionProvider implements ConnectionProvider
{
    // the name of the properties file used to get the essential values to be able to connect to the database.
    private static String PROPSFILENAME = "jbdc.properties";
    private Properties props;
    private MysqlDataSource datasource;

    /**
     * Initializes a connection from a datasource object.
     * The properties for this connection is derived from a property file. This file must be changed
     * to fit the current systems properties.
     * The filename is set in the PROPSFILENAME String above.
     */
    public MYSQLConnectionProvider(){
        props = new Properties();
        String user, password, dbhost, dbname;
        try {
            props.load(new FileInputStream(PROPSFILENAME));
            user = props.getProperty("user");
            password = props.getProperty("password");
            dbhost = props.getProperty("dbhost");
            dbname = props.getProperty("dbname");

            if(user.length()==0 || dbhost.length()==0 || dbname.length()==0){
                throw new IllegalArgumentException(PROPSFILENAME + " contains illegal values");
            }

            datasource = new MysqlDataSource();
            datasource.setUser(user);
            datasource.setPassword(password);
            datasource.setServerName(dbhost);
            datasource.setDatabaseName(dbname);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }


    @Override
    /**
     * Returns a connection from the datasource.
     * @return Connection (sql-connection)
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException{
            return datasource.getConnection();
        }
    }

