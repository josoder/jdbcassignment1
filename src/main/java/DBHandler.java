import org.h2.tools.Server;
import org.junit.Assert;

import java.sql.*;
import java.util.ArrayList;


/**
 * Class is specifically written to handle objects of the Table class,
 * writing its value to a Database.
 * @see Table
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class DBHandler
{
    // Instance variables
    // Holds a ConnectionProvider object
    private ConnectionProvider connectionProvider;
    private String tableName;

    /**
     * Constructor with 2 parameters. One is an object holding the connection, the other
     * is an object containing the table read from a file.
     * @param tableName the name of the table
     */
    public DBHandler(String tableName) {
        this.tableName = tableName;
    }

    private DBHandler(){}

    /**
     * Set the connection.
     * @param connectionProvider the connectionProvider
     */
    public void setConnectionProvider(ConnectionProvider connectionProvider){
        this.connectionProvider = connectionProvider;
    }

    /**
     * Insert the rows read from the table object.
     * Return the number of rows effected. <br>
     * This method is using a prepared statement to make it injection-safe.
     * @return (effectedRows nr of rows effected)
     */
    public int insertRowsFromTable(Table table) {
        if(table == null){
            throw new IllegalArgumentException("The table object cant be null");
        }
        ArrayList<String[]> rows = table.getRows();
        String insert = SQLHelper.insert(tableName, table.getColumnNames().length);
        String[] datatype = table.getMetaData();
        int effected=0;
        try (Connection connection = this.connectionProvider.getConnection();
             PreparedStatement insertSQL = connection.prepareStatement(insert)) {
            for (int i = 0; i < rows.size(); i++) {
                int j=0;
                for (String r : rows.get(i)) {
                    if(datatype[j].substring(0,3).equals("INT")){
                        insertSQL.setInt(j+1, Integer.parseInt(r.trim()));
                    }
                    else{
                        insertSQL.setString(j+1,r);
                    }
                    j++;
                }
                effected += insertSQL.executeUpdate();
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return effected;
    }

    /**
     * Performs an sql query selecting all rows from the db and returns the result as a
     * ArrayList of Strings. <br>
     * This method is using the .selectAll() method in SQLHelper to make it SQL-Injection safe. <br>
     * See SQLHelpers doc to learn more.
     * @see SQLHelper
     * @param table tableObject containing the values to be inserted
     * @return rList (string-list representation of the rows in the db)
     */
    public ArrayList<String> selectAllFromTable(Table table){
        Assert.assertNotNull(this.connectionProvider);
        Assert.assertNotNull(table);
        String[] columnNames = table.getColumnNames();
        String[] meta = table.getMetaData();
        String select = SQLHelper.selectAll(tableName, table.getColumnNames());

        try(Connection connection = this.connectionProvider.getConnection();
            Statement selectSQL = connection.createStatement();
            ResultSet rs = selectSQL.executeQuery(select))
        {
            ArrayList<String> rList = new ArrayList<>();
            String r="";
            for(int i=0; i<columnNames.length; i++){
                r += columnNames[i] + "  ";
            }
            r+="\n";
            rList.add(r);
            while (rs.next()) {
                r="";
                for (int i = 0; i < columnNames.length; i++) {
                    if (meta[i].substring(0, 3).equals("INT")) {
                        r += "" + rs.getInt(i+1) + "  ";
                    } else {
                        r +=  "" + rs.getString(i+1)+ "  ";
                    }
                }
                r+="\n";
                rList.add(r);
            }
            return rList;
        }
        catch (SQLException e){
            e.printStackTrace();
    }
        return null;
    }

    /**
     * Creates a new table in the db using a TableObject, returns true if successful. <br>
     * This method is using the .createTable() method in SQLHelper to make it SQL-Injection safe. <br>
     * See SQLHelpers doc to learn more.
     * @see SQLHelper
     * @param table tableObject containing the values to be inserted
     * @return boolean (successful?)
     */
    public boolean createTable(Table table){
        if(table==null){
            throw new IllegalArgumentException("Table object cant be null");
        }
        String createStmt = SQLHelper.createTable(tableName,
                table.getColumnNames(), table.getMetaData());

        System.out.println(createStmt);
        try(Connection connection = this.connectionProvider.getConnection();
            Statement createTable = connection.createStatement())
        {
            createTable.executeUpdate(createStmt);
            return true;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Drop the table(Cleaning after test).
     */
    public void removeTable(){
        String stmt = "DROP TABLE IF EXISTS " + tableName;
        try(Connection connection = this.connectionProvider.getConnection();
            Statement statement = connection.createStatement())
        {
            statement.executeUpdate(stmt);
            System.out.println("A table with the name '" + tableName + "' was removed");
        }
        catch (SQLException ex){
            System.out.print(ex);
        }
    }

}
