import jdk.nashorn.internal.codegen.TypeMap;

import javax.print.DocFlavor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.JDBCType;
import java.sql.SQLData;
import java.sql.SQLType;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.Scanner;

/**
 * Class reads a file, splits the data and inserts it into a Table object.
 * The first three rows in the file should contain the names of the columns and metadata. <br>
 * Should be in the following format: <br>
 * 1.(name of columns) columnName1/columnName2/ColumnName3 <br>
 * 2.(data type)       VARCHAR/VARCHAR/INT <br>
 * 3.(size)            100/100/10 <br>
 * followed by the values to be inserted into the columns ex. <br>
 * Name/Adress/29 <br>
 *
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class Parser {
    private Scanner scanner;
    private File file;
    private String filename;
    private Table table;
    private int columns; // for validation
    private final String ACCEPTED_TYPES[] = {"INT","VARCHAR"}; // Only accepts int and varchar

    /**
     * Constructor. Throws exception if file does not exist.
     *
     * @param filename name of the file to be read
     */
    public Parser(String filename) {
        if (filename.length() == 0) {
            throw new IllegalArgumentException("Filename cant be empty");
        }
        table = new Table();
        this.filename = filename;
        read();
    }

    /**
     * Splits the names of the columns, given as a String in the parameter and
     * inserts them into the table object.
     * Before the string is inserted it is passed to a filtering method in SQLHelper to be formatted.
     *
     * @param column name of the column
     * @see SQLHelper
     */
    private void splitColumns(String column) {
        column = SQLHelper.removeIllegalChar(column, new char[]{'/'});
        String[] columnNames = column.split("/");
        this.columns = columnNames.length;
        table.setColumnNames(columnNames);
    }

    /**
     * Reformat the metadata and inserts it into the table object.
     * Removes illegal characters before insertion and makes sure the file
     * is formatted properly.
     *
     * @param datatype String representation of the datatype
     * @param size     String representation of the size
     */
    private void splitMeta(String datatype, String size) {

        String[] metaData = datatype.split("/");
        String[] dataSize = size.split("/");
        if (metaData.length != dataSize.length || columns != metaData.length || columns != dataSize.length) {
            throw new RuntimeException("Illegal format in the metadata for the table file" +
                    "columncount / datasize / datatypes " +
                    "differs in length");
        }
        // Remove illegal characters from the file
        for (int i = 0; i < metaData.length; i++) {
            String formattedType = SQLHelper.removeIllegalChar(metaData[i]);
            String formattedSize = SQLHelper.removeIllegalChar(dataSize[i]);
            if(formattedType.equalsIgnoreCase(ACCEPTED_TYPES[0])||
                    formattedType.equalsIgnoreCase(ACCEPTED_TYPES[1])) {
                String s = formattedType + "(" + formattedSize + ")";
                metaData[i] = s;
            }
            else
                throw new RuntimeException("Illegal format in the metadata for the table file " +
                        metaData[i] + " is not a valid datatype for this file. Please use VARCHAR or INT");
        }
        table.setMetaData(metaData);
    }

    /**
     * Read the file, split the data and insert into the table object
     */
    private void read() {
        try {
            file = new File(filename);
            if (!file.exists()) {
                throw new RuntimeException("No such file");
            }

            scanner = new Scanner(file);
            if (!scanner.hasNext()) {
                throw new RuntimeException("The file is empty");
            }
            // Get the name of the columns
            splitColumns(scanner.nextLine());
            // Get meta data
            splitMeta(scanner.nextLine(), scanner.nextLine());
            // Get the rows
            ArrayList<String[]> rows = new ArrayList<>();
            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                s = SQLHelper.removeIllegalChar(s, new char[]{'/'});
                if (s.length() == 0) {
                    break;
                }
                String[] t = s.split("/");
                if (t.length != columns) {
                    throw new RuntimeException("Illegal format " + s + "\n metadata and value to be inserted " +
                            "differs in length");
                }
                rows.add(s.split("/"));
            }
            rows.trimToSize();
            table.setRows(rows);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Returns the table object
     *
     * @return table tableObject containing the values to be inserted
     */
    public Table getTable() {
        return table;
    }

}
