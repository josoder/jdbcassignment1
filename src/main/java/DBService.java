import java.util.ArrayList;

/**
 * DBService provides an abstraction of the project in a simple public interface providing the
 * client with methods to copy from the file and insert the data into a db.
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class DBService {
    private static ConnectionProvider connectionProvider;
    private static DBHandler dbHandler;
    private static Table table;
    private static Parser parser;

    /**
     * Initialize the connection
     * @param connectionProvider ConnectionProvider
     */
    private static void intConnection(ConnectionProvider connectionProvider){
        connectionProvider = new MYSQLConnectionProvider();
    }

    /**
     * Creates a table with the name given as a parameter, from the information derived from the file.
     * @see Table for more information about the structure
     * @param filename the name of the file to be read
     * @param tableName the name of the table to be created
     */
    public static void copyFile(String filename, String tableName, ConnectionProvider connectionProvider){
        intConnection(connectionProvider);
        parser = new Parser(filename);
        table = parser.getTable();
        dbHandler = new DBHandler(tableName);
        dbHandler.setConnectionProvider(connectionProvider);
        dbHandler.createTable(table);
        dbHandler.insertRowsFromTable(table);
    }

    /**
     * Performs an sql select query of all the data stored in the db.
     */
    public static void showTable(){
        ArrayList<String> select = dbHandler.selectAllFromTable(table);
        for(String row: select){
            System.out.print(row);
        }
    }

    /**
     * Returns an object containing all the data and metadata information of the table.
     * @return table (object holding all information read from the file.)
     */
    public static Table getTable(){
        return table;
    }
}
