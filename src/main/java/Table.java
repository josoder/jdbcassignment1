import java.util.ArrayList;

/**
 * Blueprint for objects holding data to be inserted into db.
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class Table
{
    // Format datatype(size) example: VARCHAR(100)
    private String[] metaData;
    private String[] columnNames;
    private ArrayList<String[]> rows;
    private String name;


    /**
     * Constructor
     */
    public Table(){
        rows = new ArrayList<>();
    }

    /**
     * Set the name of the table
     * @param name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Holds an array of Strings representing the metadata, format: type(size)
     * @param metaData
     */
    public void setMetaData(String[] metaData)
    {
        this.metaData = metaData;
    }

    /**
     * Sets columnNames, an array with the names of the columns in the database
     * @param columnNames
     */
    public void setColumnNames(String[] columnNames){

        this.columnNames = columnNames;
    }

    /**
     * Set rows, an ArrayList of String[] representing the rows in the database.
     * @param rows
     */
    public void setRows(ArrayList<String[]> rows){

        this.rows = rows;
    }

    /**
     * Returns an ArrayList of String[], each array represents a row in the db.
     * @return rows ArrayList<String[]>
     */
    public ArrayList<String[]> getRows() {

        return rows;
    }

    /**
     * Returns the metadata for each column in the format (datatype(size)) as String
     * @return metadata String[]
     */
    public String[] getMetaData(){

        return metaData;
    }

    /**
     * Returns an array of String containing the names of the columns
     * @return culumnNames String[]
     */
    public String[] getColumnNames(){

        return columnNames;
    }

    /**
     * Return the name of the table
     * @return name
     */
    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        String s = "";
        for(int i=0; i<columnNames.length; i++){
            s += String.format("%10s ", columnNames[i] + ":" + metaData[i]);
        }
        s += "\n";
        for(String[] row:rows){
            for(int i=0; i<row.length; i++) {
                s += String.format("%10s ", row[i]);
            }
            s+="\n";
        }

        return s;
    }
}
