import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Joakim Söderstrand on 2016-11-28
 * Interface for sql-connection providers.
 * @author Joakim Söderstrand 2016-11-28
 * @version 1.0
 */
public interface ConnectionProvider {
    public Connection getConnection() throws SQLException;
}
