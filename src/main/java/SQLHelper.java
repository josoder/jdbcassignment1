import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

/**
 * This is a utility class providing helpful methods for creating sqlStrings. It also removes illegal character that
 * allows for malicious code(SQL-Injections).
 * Before a statement is created with data derived from the file, it is passed through the method removeIllegalChar that
 * works like a filter for sql strings removing all illegal characters.
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class SQLHelper
{
    private SQLHelper(){}

    /**
     * Creates a sql-statement that creates the table with the table-name columns and
     * metadata given as parameters.
     * @param name name of the table
     * @param columnNames list with the columnNames
     * @param meta list with the meta-data
     * @return prepStmtCreate (String representation of prep-statement)
     */
    public static String createTable(String name,String[] columnNames, String[] meta){
        String c="CREATE TABLE "+ name + " (";
        String props="";
        char [] wList= {'(',')'};
        for(int i=0; i<columnNames.length; i++){
            props = props + removeIllegalChar(columnNames[i]) + " " +
                    removeIllegalChar(meta[i], wList);
            if(i<columnNames.length-1){
                props = props + ",";
            }
        }
        String prepStmtCreate = c + props + ")";
        return prepStmtCreate;
    }

    /**
     * Creates an sql prepared statement String that inserts a row into the table passed
     * as a parameter with the number of columns given in columnCount.
     * Example: INSERT INTO table VALUES(?,?,?,?)
     * @param name name of the table
     * @param columnCount nr of columns to be generated
     * @return insert (String representation of prep-statement)
     */
    public static String insert(String name, int columnCount){
        String insert = "INSERT INTO "+ name + " VALUES(";
        for(int i=0; i<columnCount; i++){
            insert += "?";
            if(i<columnCount-1){
                insert+=",";
            }
        }
        insert += ")";
        return insert;
    }

    /**
     * Creates an sql select-statement that selects all the columns given in the
     * columns parameter from the table given in the name parameter.
     * @param name name of the table
     * @param columns array with the columns
     * @return selectAll (String representation of the query)
     */
    public static String selectAll(String name,String[] columns){
        String selectAll = "SELECT ";
        for(int i=0;i<columns.length;i++){
            selectAll += removeIllegalChar(columns[i]);
            if(i<columns.length-1){
                selectAll += ",";
            }
        }
        selectAll += " FROM " + name;
        return selectAll;
    }

    /**
     * Remove all characters from the String except for alphabetic, digits and the
     * ones in the white-list array of chars taken as a parameter.
     * @param s The String to be formatted
     * @param whiteList the list of chars to white-list
     * @return formatted (the formatted String)
     */
    public static String removeIllegalChar(String s, char[] whiteList){
        String formatted = "";
        for(char c: s.toCharArray()) {
            for(int i=0; i<whiteList.length; i++){
                if(whiteList[i] == c) formatted += c;
            }
            if(Character.isAlphabetic(c)||Character.isDigit(c)||c==' '){
                formatted+=c;
            }
        }

        return formatted;
    }

    /**
     * Remove all characters from the String except for alphabetic and digits.
     * @param s The String to be formatted
     * @return formatted (the formatted String)
     */
    public static String removeIllegalChar(String s){
        String formatted = "";
        for(char c: s.toCharArray()) {
            if(Character.isAlphabetic(c)||Character.isDigit(c)||c==' '){
                formatted+=c;
            }
        }
        return formatted;
    }
}
