import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Main/Client class
 * @author Joakim Söderstrand 2016-12-05
 * @version 2.0
 */
public class Client
{
    public static void main(String [] args){
        // Create the table
        DBService.copyFile("file.txt", "Telefonlista", new MYSQLConnectionProvider());
        // Show the table
        System.out.println("Printing the table:");
        DBService.showTable();
        // Get the table object
        Table t = DBService.getTable();
        // Print the table object
        System.out.println('\n' + "Print the table object");
        System.out.println(t);
    }
}
