import org.junit.Assert;
import org.junit.Test;


/**
 * Created by josoder on 16.10.16.
 * Test class for Parser class.
 * The file used in this test is test2.txt and it contains the following:
 *
 *  Row 1 is the name of the columns separated with '/'.
 *  Row 2 is the sql specific data types.
 *  Row 3 is the size of the columns.
 *  The rest is rows with values to be inserted into the table.
 *
 *  Namn/Adress/Telefon/Alder
 *  VARCHAR/VARCHAR/INT/INT
 *  100/100/10/3
 *  Joakim Söderstrand/Hemlig/123456/29
 *  Test Person/Testgatan/1234567/37
 *  Per Person/Pergatan/1234569/34
 *  Anders Andersson/Andgatan/123412/39
 *  Joakim Söderstrand/Hemlig/123456/29
 *  Test Person/Testgatan/1234567/37
 *  Per Person/Pergatan/1234569/34
 *  Anders Andersson/Andgatan/123412/39
 *  Joakim Söderstrand/Hemlig/123456/29
 *  Test Person/Testgatan/1234567/37
 *  Per Person/Pergatan/1234569/34
 *  Anders Andersson/Andgatan/123412/39
 *  Joakim Söderstrand/Hemlig/123456/29
 *  Test Person/Testgatan/1234567/37
 *  Per Person/Pergatan/1234569/34
 *  Anders Andersson/Andgatan/123412/39
 */
public class ParserTest {
    public Parser cftt;
    private Table table;
    private final String FILENAME = "test2.txt";
    private final int EXPECTEDCOLUMNCOUNT = 4;
    private final int EXPECTEDROWCOUNT = 16;

    /**
     * Test that the file is copied into a table-object properly
     */
    @Test
    public void copyFileToTable(){
        cftt = new Parser(FILENAME);
        table = cftt.getTable();
        Assert.assertTrue(table.getColumnNames().length == EXPECTEDCOLUMNCOUNT);
        Assert.assertTrue(table.getRows().size() == EXPECTEDROWCOUNT);
    }

    /**
     * Try to read from a file that does not exist.
     */
    @Test(expected = RuntimeException.class)
    public void wrongFileName(){
        cftt = new Parser("Wrong");
    }
}
