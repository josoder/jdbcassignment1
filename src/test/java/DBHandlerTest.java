import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by josoder on 15.10.16.
 * Performs tests on the sql-functionallity.
 * This test is run on an in-memory database.(H2). Installed as a dependency in maven. See pom.xml.
 */
public class DBHandlerTest {
    private static H2ConnectionProvider dbConnection;
    private static DBHandler dbHandler;
    private static Parser parser;
    private static Table table;
    // Connection properties
    private static final String TABLENAME = "test";
    private static final String FILENAME = "test.txt";
    private static int nrRows;
    private static int nrColumns;


    @BeforeClass
    public static void init() throws Exception
    {
        // Set up the connection
        dbConnection = new H2ConnectionProvider();

        // Set up the tableobject
        /**
         *  To get values for testing I will use the file test.txt containing the following:
         *  ----------------------------------------
         *  Namn''/Adress'/Telefon'/Alder'
         *  VARCHAR'/'VARCHAR'/INT'/INT'
         *  100'/100'/10'/3'
         *  Joakim' Söderstrand'/Hemlig'/123456'/29'
         *  ----------------------------------------
         *  Description :
         *  The file contains values separated with "/".
         *  Row 1: Names of the columns.
         *  Row 2: Data type.
         *  Row 3: Size.
         *  Row 4: Row with values to be inserted into the table
         *
         * The file contains single quotes in the values, this is to demonstrate that this solution is safe
         * from sql-injections
         *
         *  The values will be copied with the specially designed class Parser. The
         *  class will copy the values from the file and insert it into an table object.
         */

        parser = new Parser(FILENAME);
        table = parser.getTable();
        table.setName(TABLENAME);
        nrRows = table.getRows().size();
        nrColumns = table.getColumnNames().length;
        // set up the table in the db to test with it
        dbHandler = new DBHandler("test");
        dbHandler.setConnectionProvider(dbConnection);
        dbHandler.createTable(table);
    }

    /**
     * Insert the rows copied from the file into the table-object, to the db.
     * Check that the nr of rows effected is equal to the actual nr of rows in the file.
     */
    @Test
    public void insertRows(){
        int rowsEffected = dbHandler.insertRowsFromTable(table);
        Assert.assertTrue(rowsEffected == nrRows);
    }

    /**
     * Make sure exception is thrown when trying to insert null-object.
     */
    @Test(expected = IllegalArgumentException.class)
    public void insertWithNullObject(){
        int wontWork = dbHandler.insertRowsFromTable(null);
    }

    /**
     * Select all data from the table, makes sure that rowcount from the db matches the files.
     */
    @Test
    public void select(){
        insertRows();
        ArrayList<String> select = dbHandler.selectAllFromTable(table);
        System.out.println(select);
        Assert.assertTrue(select.size()==nrRows+1); // Will return the rows as string + the columns as a string
    }

    @AfterClass
    public static void clean(){
        dbHandler.removeTable();
    }
}
