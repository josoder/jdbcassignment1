import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by josoder on 15.10.16.
 * Testing the sql Connection Provider.
 */
public class DBConnectionTest {

    MYSQLConnectionProvider dSource;
    Connection connection;

    /**
     * Creates an object and calls the getConnection(), to get a connection to the db.
     */
    @Test()
    public void testConnection(){
        try {
            dSource = new MYSQLConnectionProvider();
            connection = dSource.getConnection();
        }
        catch (SQLException e){
            connection = null;
        }
        Assert.assertNotNull(connection);
    }


}
