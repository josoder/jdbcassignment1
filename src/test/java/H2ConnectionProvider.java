import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * H2 , in memory db-connection provider.
 * @author Joakim Söderstrand on 2016-11-28
 * @verstion 1.0
 */
public class H2ConnectionProvider implements ConnectionProvider {
    @Override
    public Connection getConnection() throws SQLException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            throw new SQLException("Unable to find H2 driver");
        }
        return DriverManager.getConnection("jdbc:h2:~/lab", "dummy", "dummy");
    }
}
