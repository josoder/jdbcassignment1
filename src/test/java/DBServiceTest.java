import org.h2.tools.Server;
import org.junit.*;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by josoder on 08.10.16.
 * This test is run on an in-memory database.(H2). Installed as a dependency in maven. See pom.xml.
 * Test for the public interface provided by the DBService class.
 */
public class DBServiceTest
{
    private static H2ConnectionProvider dbConnection;
    private static Table table;
    private static Parser parser;
    private static DBHandler dbHandler;
    private static int nrRows, nrColumns;
    private static Server server;

    // Properties for connection
    private static final String TABLENAME = "test";
    private static final String FILENAME = "test.txt";

    @BeforeClass
    public static void init() throws Exception {
        // Set up the connection provider
        dbConnection = new H2ConnectionProvider();

        // Set up the tableobject
        /**
         *  To get values for testing I will use the file test.txt containing the following:
         *  ----------------------------------------
         *  Namn''/Adress'/Telefon'/Alder'
         *  VARCHAR'/'VARCHAR'/INT'/INT'
         *  100'/100'/10'/3'
         *  Joakim' Söderstrand'/Hemlig'/123456'/29'
         *  ----------------------------------------
         *  Description :
         *  The file contains values separated with "/".
         *  Row 1: Names of the columns.
         *  Row 2: Data type.
         *  Row 3: Size.
         *  Row 4: Row with values to be inserted into the table
         *
         * The file contains single quotes in the values, this is to demonstrate that this solution is safe
         * from sql-injections
         *
         *  The values will be copied with the specially designed class Parser. The
         *  class will copy the values from the file and insert it into an table object.
         */

        parser = new Parser(FILENAME);
        table = parser.getTable();
        table.setName(TABLENAME);
        nrRows = table.getRows().size();
        nrColumns = table.getColumnNames().length;
        server = Server.createTcpServer("-tcpPort", "9123", "-tcpAllowOthers").start();
    }


    /**
     *  Read the file, create the table-object and insert the information into the db.
     */
    @Test
    public void setUpTable(){
       DBService.copyFile(FILENAME, TABLENAME, dbConnection);
    }

    /**
     * Show the data inserted into the db.
     */
    @Test
    public void selectTable()
    {
        DBService.showTable();
    }

    @AfterClass
    public static void clearDb(){
        try(Statement st = dbConnection.getConnection().createStatement()){
            st.execute("DROP TABLE " + TABLENAME);}
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}